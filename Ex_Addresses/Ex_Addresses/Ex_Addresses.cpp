#include "Ex_Addresses.h"
#include <iostream>

namespace Ex
{
    void VisualizeAddressesInMemory()
    {
        auto a = 5;
        auto b = 5.0;
        const auto cb = 5.0;
        auto c = 6L;
        auto d = 7U;
        auto e = 'a';
        auto f = "str";

		std::cout << "The value of a is " << a << " The address of it is " << &a << std::endl;
		std::cout << "The value of b is " << b << " The address of it is " << &b << std::endl;
		std::cout << "The value of cb is " << cb << " The address of it is " << &cb << std::endl;
		std::cout << "The value of c is " << c << " The address of it is " << &c << std::endl;
		std::cout << "The value of d is " << d << " The address of it is " << &d << std::endl;
		std::cout << "The value of e is " << e << " The address of it is " << &e << std::endl;
		std::cout << "The value of f is " << f << " The address of it is " << &f << std::endl;


        // 1.0 What are the addresses of the previous declared variables? (using VS Debug)
		/*
		+&a	0x0117f668 {5}	int *
		+&b	0x0117f658 {5.0000000000000000}	double *
		+&cb	0x0117f648 {5.0000000000000000}	const double *
		+&c	0x0117f63c {6}	long *
		+&d	0x0117f630 {7}	unsigned int *
		+&e	0x0117f627 "a��������\a"	char *
		+&f	0x0117f618 {0xcccccccc <Error reading characters of string.>}	const char * *
		*/

        // 1.1 Run the program multiple times, do their address change ? Why ?
		//The addresses change because the memory is allocated at each run. 

        // 2. What are the sizes of the variables previusly declared ?
		/*		
		sizeof(a)	4	unsigned int
		sizeof(b)	8	unsigned int
		sizeof(cb)	8	unsigned int
		sizeof(c)	4	unsigned int
		sizeof(d)	4	unsigned int
		sizeof(e)	1	unsigned int
		sizeof(f)	4	unsigned int
		*/

        // 3. What are the sizes of their addresses ?
		//Depends on the operating system...4 bytes for 32 bits and 8 bytes for 64 bits

        // 4. Write a function that display <variable_value> : <variable_address> and use it to display information for previously declared variables

        // 5. Change target platform for compiling. What is the difference between x86 & x64?

        // 6.0 Swap the contents of two integer variables.
        // 6.1 Write a function that does that.

    }
}